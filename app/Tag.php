<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Models\Media;

class Tag extends Model implements HasMedia
{
    use SoftDeletes, HasMediaTrait;

    public $table = 'tags';

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $appends = [
        'tag_logo_image',
        'tag_background_image',
    ];

    protected $fillable = [
        'name',
        'slug',
        'about',
        'intro',
        'active',
        'cta_title',
        'updated_at',
        'created_at',
        'restricted',
        'deleted_at',
        'is_popular',
        'is_featured',
        'tag_fg_color',
        'tag_bg_color',
        'cta_subtitle',
        'popular_order',
        'featured_order',
        'submission_guidelines',
    ];

    public function registerMediaConversions(Media $media = null)
    {
        $this->addMediaConversion('thumb')->width(50)->height(50);
    }

    public function getTagLogoImageAttribute()
    {
        $file = $this->getMedia('tag_logo_image')->last();

        if ($file) {
            $file->url       = $file->getUrl();
            $file->thumbnail = $file->getUrl('thumb');
        }

        return $file;
    }

    public function getTagBackgroundImageAttribute()
    {
        $file = $this->getMedia('tag_background_image')->last();

        if ($file) {
            $file->url       = $file->getUrl();
            $file->thumbnail = $file->getUrl('thumb');
        }

        return $file;
    }

    public function users()
    {
        return $this->belongsToMany(User::class);
    }

}
