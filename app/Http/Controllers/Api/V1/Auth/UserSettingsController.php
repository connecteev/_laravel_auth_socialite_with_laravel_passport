<?php

namespace App\Http\Controllers\Api\V1\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Http\Resources\Admin\UserResource;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Validation\Rule;

class UserSettingsController extends Controller
{
    /**
     * Update the user's profile information.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $this->validate($request, [
            'userId' => 'required',
            'userOriginalUsername' => 'required',
            // ignore current user ID during the unique email check
            'email' => ['required', 'email', Rule::unique('users')->ignore($request->userId)], // 'email|unique:users',

            // ignore current user ID during the unique username check
            'username' => ['required', 'regex:/^[a-zA-Z0-9.]+$/i', Rule::unique('users')->ignore($request->userId)], //'alpha_num',
        ]);

        $user = User::where([
            ['id', '=', $request->userId],
            ['username', '=', $request->userOriginalUsername],
            ['deleted_at', '=', NULL],
        ])
            ->select('id', 'email', 'username')
            // ->with([
            //     // Note: must also include the user_id field above, otherwise the user object returned will be null
            //     'profiles' => function ($query) {
            //         $query
            //             ->where([
            //                 ['deleted_at', '=', NULL],
            //             ])
            //             // ->select('id', 'username')
            //         ;
            //     }
            // ])
            ->find(1);

        if (!$user) {
            return response(null, Response::HTTP_BAD_REQUEST);
        }

        $saveData = false;
        if (isset($request->username) && ($request->username != $user->username)) {
            $user->username = $request->username;
            $saveData = true;
        }
        if (isset($request->email) && ($request->email != $user->email)) {
            $user->email = $request->email;
            $saveData = true;
        }

        if ($saveData) {
            $user->save();
            // $user->update($request->only(['username', 'email']));
            return new UserResource($user);
            // return tap($user)->update($request->only('username', 'email'));
        }
        return response(null, Response::HTTP_NO_CONTENT);
    }
}
