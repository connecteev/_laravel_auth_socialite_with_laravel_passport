<?php

namespace App\Http\Controllers\Api\V1\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\User;
use Illuminate\Support\Facades\Hash;
use Route;

class PassportAuthController extends Controller
{
    public function local()
    {
        return response()->json(['user' => auth()->user()]);
    }

    public function oauth()
    {
        return response()->json(auth()->user());
    }


    // From Andre Madarang's secure version of register / login with Laravel Passport, where secret key is NOT exposed
    // Original code at https://github.com/drehimself/todo-laravel.git (and front-end at https://github.com/drehimself/todo-vue.git)
    public function login(Request $request)
    {
        $params = [
            'grant_type'    => 'password',
            'client_id'     => config('services.passport.client_id'),
            'client_secret' => config('services.passport.client_secret'),
            'scope'         => '*',
            'username'      => $request->username,
            'password'      => $request->password,
        ];

        // Andre Madarang's original code at https://github.com/drehimself/todo-laravel.git (and front-end at https://github.com/drehimself/todo-vue.git)
        // Note: Use this approach because his guzzlehttp version doesnt work on localhost with artisan serve (apparently it will if using nginx or apache)

        // Forward the request to the oauth token request endpoint
        $request->request->add($params);
        $tokenRequest = Request::create(config('services.passport.login_endpoint'), 'post'); // the default login_endpoint is '/oauth/token'
        return Route::dispatch($tokenRequest);
    }

    // From Andre Madarang's secure version of register / login with Laravel Passport, where secret key is NOT exposed
    // Original code at https://github.com/drehimself/todo-laravel.git (and front-end at https://github.com/drehimself/todo-vue.git)
    public function register(Request $request)
    {
        $request->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            //'password' => 'required|string|min:6|confirmed', // if you want to add a confirm password field
            'password' => 'required|string|min:6',
        ]);

        return User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
        ]);
    }

    // From Andre Madarang's secure version of register / login with Laravel Passport, where secret key is NOT exposed
    // Original code at https://github.com/drehimself/todo-laravel.git (and front-end at https://github.com/drehimself/todo-vue.git)
    public function logout()
    {
        auth()->user()->tokens->each(function ($token, $key) {
            //$token->revoke(); // if you want to mark the token in the oauth_access_tokens table as 'revoked'
            $token->delete();
        });

        return response()->json('Logged out successfully', 200);
    }
}
