# --------------------------------------------------
#
#   envoy run local     # builds for local
#
# --------------------------------------------------
#
@servers(['localhost' => 'localhost'])

@setup
@endsetup

@story('local')
    _local_build
@endstory

@task('_local_build')
    echo "local build: started"
#    rm -rf composer.lock
#    composer install
    php artisan key:generate
    php artisan migrate:fresh
    php artisan db:seed
    php artisan passport:install
    php artisan storage:link
    echo "Clearing cache.."
    php artisan config:clear
    php artisan cache:clear
    composer dump-autoload
    php artisan view:clear
    php artisan route:clear
    echo "local build: done"
@endtask
