<?php

use App\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    public function run()
    {
        $users = [
            [
                'id'             => 1,
                'name'           => 'Admin',
                'email'          => 'admin@admin.com',
                'password'       => '$2y$10$HFoHf6lLlpvEbMv7WKpLFucWFpTCAVBee6tnEu764kOTc/dLmKqzS',
                'remember_token' => null,
                'created_at'     => '2019-10-04 01:54:02',
                'updated_at'     => '2019-10-04 01:54:02',
                'username'       => '',
            ],
        ];

        User::insert($users);
    }
}
