<?php

use App\Role;
use App\User;
use Illuminate\Database\Seeder;
use Faker\Generator as Faker;

// Call using command: php artisan db:seed --class=AdminUsersTableSeeder
class AdminUsersTableSeeder extends Seeder
{
    public function run(Faker $faker)
    {
        $adminRole = Role::where([
            'title' => 'Admin',
        ])->firstOrFail();

        factory(User::class, 1)
            ->create(
                [
                    'id'                => 1,
                    'email'             => env('ADMIN_EMAIL', 'admin@admin.com'),
                    'username'          => env('ADMIN_USERNAME', 'admin'),
                    'password'          => Hash::make(env('ADMIN_PASSWORD', 'admin')),
                    'email_verified_at' => now(),
                    'remember_token'    => Str::random(10),
                    'created_at'        => now(),
                    'updated_at'        => now(),
                ],
            )
            ->each(function ($user) use ($faker, $adminRole) {
                // populate the role_user table
                $user->roles()->sync($adminRole->id);
            });
    }
}
