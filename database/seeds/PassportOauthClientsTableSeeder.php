<?php

use Illuminate\Database\Seeder;

use Faker\Generator as Faker;

// Seeds oauth_clients table for Laravel Passport Authentication
// Call using command: php artisan db:seed --class=PassportOauthClientsTableSeeder
class PassportOauthClientsTableSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run(Faker $faker)
    {
        echo "Seeding oauth_clients table for Laravel Passport Auth... \n";
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');

        $oauth_clients = [
            [
                'user_id'                   => NULL,
                'name'                      => env('PASSPORT_PERSONAL_ACCESS_CLIENT_NAME'),
                'id'                        => env('PASSPORT_PERSONAL_ACCESS_CLIENT_ID'),
                'secret'                    => env('PASSPORT_PERSONAL_ACCESS_CLIENT_SECRET'),
                'redirect'                  => env('PASSPORT_PERSONAL_ACCESS_CLIENT_REDIRECT_URL'),
                'personal_access_client'    => 1,
                'password_client'           => 0,
                'revoked'                   => 0,
                'created_at'                => now(),
                'updated_at'                => now(),
            ],
            [
                'user_id'                   => NULL,
                'name'                      => env('PASSPORT_PASSWORD_GRANT_NAME'),
                'id'                        => env('PASSPORT_PASSWORD_GRANT_CLIENT_ID'),
                'secret'                    => env('PASSPORT_PASSWORD_GRANT_CLIENT_SECRET'),
                'redirect'                  => env('PASSPORT_PASSWORD_GRANT_CLIENT_REDIRECT_URL'),
                'personal_access_client'    => 0,
                'password_client'           => 1,
                'revoked'                   => 0,
                'created_at'                => now(),
                'updated_at'                => now(),
            ],
        ];

        DB::table('oauth_clients')->insert($oauth_clients);

        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}
