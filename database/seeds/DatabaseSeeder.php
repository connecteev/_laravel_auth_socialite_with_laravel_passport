<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    public function run()
    {
        if (App::Environment() === 'local') {
            echo "On Local Environment: Refreshing all Database Tables\n";
            \Artisan::call('migrate:fresh');
        }
        $this->call([
            PassportOauthClientsTableSeeder::class,
            PermissionsTableSeeder::class,
            RolesTableSeeder::class,
            PermissionRoleTableSeeder::class,
            // UsersTableSeeder::class,
            AdminUsersTableSeeder::class,
            // RoleUserTableSeeder::class,
            TagsTableSeeder::class,
        ]);

        if (App::Environment() === 'local' || App::Environment() === 'dev') {
            $this->seedLocalOrDevDB();
        }
    }

    private function seedLocalOrDevDB()
    {
        if (App::Environment() !== 'local' && App::Environment() !== 'dev') {
            echo "Not local or Dev DB. Quitting.";
            return;
        }
        echo "On Local Environment: Calling Local DB Scripts to set up Test Data\n";

        // create test users
        $this->call(UsersFakeDataSeeder::class);

    }
}
