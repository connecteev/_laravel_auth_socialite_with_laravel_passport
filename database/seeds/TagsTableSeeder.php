<?php

use App\Tag;
use Illuminate\Database\Seeder;

class TagsTableSeeder extends Seeder
{
    public function run()
    {
        $this->addFeaturedTags();
    }

    // these are for the tag streams that get shown on the homepage / feed's right rail
    private function addFeaturedTags()
    {
        $currentTimestamp = date("Y-m-d H:i:s");
        $featuredTags = [
            [
                "name"              => "Help",
                "slug"              => "help",
                "active"            => 1,
                "is_featured"       => 1,
                "featured_order"    => 1,
                "tag_bg_color"      => "#4fa93a",
                "tag_fg_color"      => "#ffffff",
                "cta_title"         => "Ask for Help",
                "cta_subtitle"      => "helo each other out.",
                "created_at"        => $currentTimestamp,
                "updated_at"        => $currentTimestamp,
                "deleted_at"        => null,
            ],
            [
                "name"              => "AMA",
                "slug"              => "ama",
                "active"            => 1,
                "is_featured"       => 1,
                "featured_order"    => 2,
                "tag_bg_color"      => "#4594ba",
                "tag_fg_color"      => "#ffffff",
                "cta_title"         => "Start an \"AMA\"",
                "cta_subtitle"      => "Everybody has a story to tell.",
                "created_at"        => $currentTimestamp,
                "updated_at"        => $currentTimestamp,
                "deleted_at"        => null,
            ],
            [
                "name"              => "Challenge",
                "slug"              => "challenge",
                "active"            => 1,
                "is_featured"       => 1,
                "featured_order"    => 3,
                "tag_bg_color"      => "#f1760e",
                "tag_fg_color"      => "#ffffff",
                "cta_title"         => "Create a Challenge",
                "cta_subtitle"      => "Flex your skills, stay sharp.",
                "created_at"        => $currentTimestamp,
                "updated_at"        => $currentTimestamp,
                "deleted_at"        => null,
            ],
            [
                "name"              => "Discuss",
                "slug"              => "discuss",
                "active"            => 1,
                "is_featured"       => 1,
                "featured_order"    => 4,
                "tag_bg_color"      => "#2192de",
                "tag_fg_color"      => "#ffffff",
                "cta_title"         => "Start a Discussion",
                "cta_subtitle"      => "What does the community think?",
                "created_at"        => $currentTimestamp,
                "updated_at"        => $currentTimestamp,
                "deleted_at"        => null,
            ],
        ];

        Tag::insert($featuredTags);
    }

}
