<?php

use Illuminate\Database\Seeder;

use App\Role;
use App\User;
use Faker\Generator as Faker;

// Call using command: php artisan db:seed --class=UsersFakeDataSeeder
class UsersFakeDataSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run(Faker $faker)
    {
        echo "Seeding Fake User data... \n";

        DB::statement('SET FOREIGN_KEY_CHECKS=0;');

        $userRole = Role::where([
            'title' => 'User',
        ])->firstOrFail();

        // use Faker to create random users
        // Note: need a for loop to check for unique usernames and emails, otherwise could have instead used: factory(User::class, 400)
        for ($i = 0; $i < 20; $i++) {
            $fakeUserName = $faker->userName;
            while (User::where('username', '=', $fakeUserName)->exists()) {
                // echo "username $fakeUserName exists, attempt to find a unique username..\n";
                $fakeUserName = $faker->userName;
            }
            $fakeEmail = $faker->unique()->safeEmail;
            while (User::where('email', '=', $fakeEmail)->exists()) {
                // echo "email $fakeEmail exists, attempt to find a unique email..\n";
                $fakeEmail = $faker->unique()->safeEmail;
            }
            factory(User::class, 1)
                ->create([
                    'username' => $fakeUserName,
                    'email' => $fakeEmail,
                ])
                ->each(function ($user) use ($faker, $userRole) {
                    // populate the role_user table
                    $user->roles()->sync($userRole->id);
                });
        }

        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}
